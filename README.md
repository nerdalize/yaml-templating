Experiment with flexible yaml templating.

Goal is to create a data structure and templating format that can be used to flexibly 
generate documents (Terraform documents, Grafana dashboards, etc), without writing
procedural code. Honestly, I'm not sure if this is a great idea. 

Ideas include: 
- data structures in yml
- metadata annotations to create a data inheritance hierarchy, allowing for easily 
  modifiable defaults
- programmable jinja2 templating language

