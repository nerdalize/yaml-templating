#!/usr/bin/env python
helptext=""" 
  Generate json file based on yml context and template.
"""

import jinja2

import yaml
import argparse
import sys
import traceback
import json
import copy
import itertools
import os
import re

def die(s):
  sys.stderr.write("Error: %s\n" % s)
  sys.exit(1)

def assert_context(c, field):
  if not field in c: 
    die("The context file does not specify a '%s' variable" % field)

def template_jinja_file(filename, context):
    my_filters = { 'json': json.dumps, 'yaml_load' : yaml.load }
    env = jinja2.environment.Environment(undefined=jinja2.StrictUndefined, extensions=['jinja2.ext.with_'])
    env.filters.update(my_filters)
        
    env.loader = jinja2.FileSystemLoader('.')
    try:
        tmpl = env.get_template(filename)
    except jinja2.exceptions.TemplateNotFound as e:
        die("Template not found: "+str(e))
    except Exception as e:
        error = "Cannot template file %s:\n%s " % (filename, get_exception_error_string(regexp=r".*in template$"))
        die(error)
    
    try:
        r = tmpl.render(context)
        return r
    except jinja2.exceptions.UndefinedError as e:
        error = "Cannot template file %s:\n%s " % (filename, get_exception_error_string(regexp=r".*reraise"))
        die(error)
    except TypeError as e:
        error = "Cannot template file (undefined value?) %s:\n%s " % (filename, get_exception_error_string(regexp=r".*reraise"))
        die(error)
        
def main():

    parser = argparse.ArgumentParser(description=helptext)
    parser.add_argument('input', type=str, help='a file that is to be templated')
    parser.add_argument('-c', '--context-file', metavar='contextfile', nargs='*', type=str, help='a list of context files with variables. ')
    parser.add_argument('-C', '--context-var', metavar='key=value', nargs='*', type=str, help='a list of key=value pairs to override in context')
    #parser.add_argument('-r', action='store_true', help='replace intermediate yml file if it already exists')
    parser.add_argument('-p', action='store_true', help='preprocess input file as jinja2, although it does not have .j2 extension')
    parser.add_argument('-s', '--select', metavar='field', type=str, help='output only given key from the result')
    
    args = parser.parse_args()
    
    context = {
        
    }
    
    template_outfile = None # the name of the file that is considered preprocessed by jinja2
    
    # read all --context files and merge them (non-recursively) in context
    if args.context_file:
        for cfile in args.context_file:
            if cfile=='-':
                cpart = get_yaml_from_stream(sys.stdin)
            else:
                cpart = get_yaml_from_file(cfile)
            if not type(cpart)==dict:
                die("the context file %s contains a %s type, not dict" % (cfile, type(cpart) ) )
            context.update(cpart)
    
    # set commandline -C k=v arguments in context
    if args.context_var:
        context_var_dict = {}
        for kvpair in args.context_var:
            k,v = kvpair.split('=')
            context_var_dict[k] = v
        # why mod here? 
        #mod = modify_special_fields(context_var_dict)
        context.update(context_var_dict)
    
    
    #if (not args.r) and os.path.isfile(processed_file):
    #    die("The intermediate yml file '%s' already exists" % processed_file)
    # preprocess as jinja2, then output as yaml
    
    r = template_document_and_fields(args.input, context)
    
    if args.select:
        if args.select in r:
            r = r[args.select]
        else:
            r = None
            
    print(json.dumps(r))

def add_jinja2_functions(context):
    """set custom jinja2 functions that we defined in the context, so they are available for use in template.
       Note the names can collide with names imported; the imported names override the function names, 
       so we only set the function if it is not imported as var. """
       
    for name, func in j2functions.iteritems():
        if name not in context:
            context[name] = func
        
def template_document_and_fields(filename, context):
    add_jinja2_functions(context)
    
    processed_file = filename + "-jinja-out-"+str(generate_id('processed_files'))
    rendered_yml = template_jinja_file(filename, context)
    with open(processed_file, 'w') as yml_stream:
        yml_stream.write(rendered_yml)
    data = get_yaml_from_file(processed_file)
    
    # succeeded without crash, remote processed file
    os.remove(processed_file)

    # note that Go Template has some context-awareness built-in (the . field for a range, for example)
    # maybe it is nice to use Go Template, for that reason. 
    
    # first replace the strings. We could do this in the template_object function, but this is conceptually a bit cleaner
    # as we whould arguably have replaced the stuff after doing the jinja2 preprocessing on the document. 
    data = replace_strings_in_nested_dict(data)
            
    r = template_object(data, context)
    #r = modify_special_fields(r)
    return r

def replace_strings_in_nested_dict(data):
    """ replace "<<" / ">>" strings by "{{" / "}}" in a nested dict """
    recurse = replace_strings_in_nested_dict    
    if type(data)==str:
        return data.replace("<<", "{{").replace(">>", "}}")
    elif type(data)==list:
        return [ recurse(elem) for elem in data ]
    elif type(data)==dict:
        return { k:recurse(v) for (k,v) in data.iteritems() }
    else:
        # primitive type like int? 
        return data

def template_object(data, context, path=[]):
    """process template string 'data' with given context.
    Path is given for debugging and error messages, indicating where in the original datastructure we are located. """
    
    def create_local_context(context, data):
        """return a deep copy of 'context' with additional value context['_']==data """
        c = copy.deepcopy(context)
        c['_'] = data
        return c
            
    if type(data)==str:
        return template_from_string(data, context, path)
    elif type(data)==list:
        # we need the index value i to update the path
        return [ template_object(elem, context, path + [str(i)] ) for i,elem in enumerate(data) ]
    elif type(data)==dict:
    
        # first template all strings deep in the dict        
        # we need the key k to update the path
        f = lambda k,v: template_object(v, create_local_context(context, data), path=path+[k] )
        data_formatted = { k:f(k,v)  for (k,v) in data.iteritems() }
        
        # remove private fields (keys starting with '_')
        #data_formatted = { k:v for (k,v) in data_formatted.iteritems() if not k.startswith('_') }
        # 
        data_filter_applied = {}
        
        # do not use iteritems(), as we may be modifying the original dict in-loop
        keys_done = set()
        
        for k,v in data_formatted.items(): 
            
            # a key 'mykey' may have been marked as done, if we already processed a key 'mykey|somefilter'. 
            # the filtered key overrides the other one. 
            if k in keys_done:
                continue
            
            keys_done.add(k)
                
            if not ('|' in k):
                data_filter_applied[k] = v
            else:
                newkey, filter_str = k.split('|')
                f_spec = filter_str.split(':')
                filter_name = f_spec[0]
                filter_arg = f_spec[1] if len(f_spec)>1 else None
                if filter_name in filters:
                    filter_func = filters[filter_name]
                else:
                    die("illegal filter name: '%s'" % filter_name)
                
                # apply filter func
                if filter_arg != None:
                    transformed_v = filter_func(v, filter_arg)
                else:
                    transformed_v = filter_func(v)
                
                if newkey=='<<':
                    # absorb the value, which should be a dict, into the parent 'cast' dict. 
                    if not type(transformed_v)==dict:
                        raise Exception("Illegal value type for '<<' key at path '%s': need dict\n    " % ".".join(path))
                    data_filter_applied.update(transformed_v)
                else:
                    data_filter_applied[newkey] = transformed_v
                    keys_done.add(newkey)
                    
        return data_filter_applied
    else:
        # primitive type like int? 
        return data

def filter_combine(d):
    """ convert a dict of lists to a list of dicts, combining all elements of input lists. 
     
        {k1: [A,B], k2: [a,b]} => 
        [ {k1:A, k2:a}, {k1:A, k2:b}, {k1:B, k2:a}, {k1:B, k2:b} ]
    """
    if type(d)!=dict:
        die("|combine filter received argument of type %s (should be dict)" % type(d))
    keys = d.keys()
    values = d.values() # values must a list of lists
    return  [ dict(itertools.izip(keys, x)) for x in itertools.product(*values) ]

def filter_template(data, template_name):

    # special func: template the given data dict with the template of the argument, and insert
    # in parent dict
    r = template_document_and_fields(template_name, data)
    return r
    #y = get_yaml_from_stream(t, "Invalid yaml returned by template %s" % template_name)
    #return y
                
filters = {
    'int' : int,
    'float': float,
    'combine' : filter_combine,
    'template': filter_template
}

def modify_special_fields(data):
    """modify special fields:
       - remove all private fields in a nested dict. 
         private fields have a key starting with '_'. Maybe we want to make this prefix configurable later, 
         because this functionality disallows intended _ prefixes. But for now it's hard.
       - cast strings that have a keyname with '|' char. e.g. { 'id|int' : "12" } => { 'id' : 12 }
       
       """
    if type(data)==list:
        return [ modify_special_fields(elem) for elem in data ]
    elif type(data)==dict:
        priv_removed = { k:modify_special_fields(v) for (k,v) in data.iteritems() if not k.startswith('_') }
        cast = {}
        
        for k,v in priv_removed.iteritems():
            if '|' in k:
                newkey, filter_str = k.split('|')
                f_spec = filter_str.split(':')
                filter_name = f_spec[0]
                filter_arg = f_spec[1] if len(f_spec)>1 else None
                if filter_name in filters:
                    filter_func = filters[filter_name]
                else:
                    die("illegal filter name: '%s'" % filter_name)
                
                # apply filter func
                if filter_arg != None:
                    transformed_v = filter_func(v, filter_arg)
                else:
                    transformed_v = filter_func(v)
                
                if newkey=='<<':
                    # absorb the value, which should be a dict, into the parent 'cast' dict. 
                    if not type(transformed_v)==dict:
                        raise Exception("Type is not a dict")
                    cast.update(transformed_v)
                else:
                    cast[newkey] = transformed_v
            else:
                cast[k] = v
        return cast
        
    else:
        # primitive type like int? 
        return data


def template_from_string(data, context, path):

   # if type(data) == str:
   #     data = unicode(data, 'utf-8')

    def my_finalize(thing):
        return thing if thing is not None else ''

    environment = jinja2.Environment(trim_blocks=True, undefined=jinja2.StrictUndefined, extensions=[], finalize=my_finalize)
    #environment.filters.update(_get_filters())
    #environment.template_class = J2Template

    # 6227
    if isinstance(data, unicode):
        try:
            data = data.decode('utf-8')
        except UnicodeEncodeError, e:
            pass

    try:
        template = environment.from_string(data)
    except Exception as e:
        raise e
    except Exception, e:
        if 'recursion' in str(e):
            raise errors.AnsibleError("recursive loop detected in template string: %s" % data)
        else:
            return data
      
    if not type(context)==dict:
        print("Cannot load context")
    try: 
        result = template.render(context)
#        if type(result)!=str and type(result)!=unicode:
#            print("Input:  '%s' - type '%s'" %( data, type(data)))
#            print("Output: '%s' - type '%s'" %( result, type(result)))
        return str(result)
    except jinja2.exceptions.UndefinedError as e:
        error = ("Cannot template element at path '%s':\n    " % ".".join(path) ) \
                + get_exception_error_string() \
                + "You must define the missing variable (in your context file, or in the rest of the datastructure)"
            
        die(error)


def get_exception_error_string(regexp="^[^\s]"):
    """ An exception first contains a Python stacktrace, and then the error string. 
        The error string may contain a parsing stacktrace for yml/jinja2/etc. We want only the latter part (starting 
        with the error name, up to the end)
        
        regexp is the regexp that you want the stracktrace to start with, IGNORING the 
        first "Traceback (most recent call last):" line. 
        
        """
   
    rx = re.compile(regexp)
    
    
    lines=traceback.format_exc().split('\n')
    while len(lines)>0:
        if lines[0].startswith("Traceback (most recent call last):"):
            lines = lines[1:] # drop this line
        elif rx.match(lines[0])==None:
            lines = lines[1:] # drop this line
        else:
            # the first line that is not part of the traceback
            return '\n'.join(lines)
            
    # nothing left, this will never help. Return everything instead. 
    
    return traceback.format_exc()+"\n Could not find the right portion of the trace, so I gave you all we've got."
     

def get_yaml_from_stream(stream, fail_msg="Cannot parse yaml from internal stream"):
    try: 
        data = data = yaml.load(stream)
        return data
    except yaml.scanner.ScannerError:
        error = get_exception_error_string()
        die ("%s: \n%s" % (fail_msg, error))
    except yaml.constructor.ConstructorError:
        error = get_exception_error_string()
        die ("%s: \n%s" % (fail_msg, error))
    except yaml.parser.ParserError:
        error = get_exception_error_string()
        die ("%s: \n%s" % (fail_msg, error))

        
def get_yaml_from_file(fname):
    with open(fname, "r") as stream:
        return get_yaml_from_stream(stream, fail_msg="It seems the file %s is not valid yaml" % fname)
        
    
############ jinja2 custom functions


ids = {} # mapping of categories to id counters
def generate_id(category='default'):
    """ function for in the jinja2 template to generate unique id's """
    if not (category in ids):
        ids[category] = 0
    c = ids[category]
    ids[category] = c + 1
    
#    sys.stderr.write("generated id %s for %s\n" % (str(c), category ))
    return c
    
    
j2functions = {
    'generate_id' : generate_id
}
    
    
            
if __name__=="__main__":
  main()


